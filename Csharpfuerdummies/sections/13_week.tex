\pagebreak
\section{Reflection}
Unter Reflection versteht man die Analyse von Metadaten eines Objekts zur Laufzeit. Mit Reflection lassen sich Typen suchen und instanziieren. Die abstrakte Basisklasse System.Type repräsentiert einen Typen. System.RuntimeType erbt von System.Type. Mit Reflection können auch \lstinline{private} Felder gelesen und geschrieben werden.

\subsection{Type-Discovery}
Suche aller Typen in einem Assembly.

\begin{lstlisting}
Assembly a01 = Assembly.Load("mscorlib");

Type[] t01 = a01.GetTypes(); 
foreach (Type type in t01) {
	Console.WriteLine(type); 
	MemberInfo[] mInfos = type.GetMembers();
	foreach (var mi in mInfos) {
		Console.WriteLine(
			"\t{0}\t{1}",
			mi.MemberType,
			mi);
	}
}
\end{lstlisting}

\subsection{Member auslesen}

\begin{lstlisting}
Type type = typeof(Counter); 
MemberInfo[] miAll = type.GetMembers(); 
foreach (MemberInfo mi in miAll) {
	Console.WriteLine(
	"{0} is a {1}",
	mi, mi.MemberType);
}
Console.WriteLine("----------");
PropertyInfo[] piAll = type.GetProperties();
foreach (PropertyInfo pi in piAll) {
	Console.WriteLine(
	"{0} is a {1}",
	pi, pi.PropertyType);
}

// Filter members according to BindingFlag or FilterName
Type type = typeof(Assembly); BindingFlags bf =
   BindingFlags.Public |
   BindingFlags.Static |
   BindingFlags.NonPublic |
   BindingFlags.Instance |
   BindingFlags.DeclaredOnly;

System.Reflection.MemberInfo[] miFound = type.FindMembers(
	MemberTypes.Method, bf, Type.FilterName, "Get*"
);
\end{lstlisting}

\pagebreak
\subsection{Field Information}
Die Field Info beschreibt ein Feld einer Klasse (Name, Typ, Sichtbarkeit). Die Felder können mit \lstinline{object GetValue(object obj)} und \lstinline{void SetValue(object obj, object value)} auch gelesen und geschrieben werden.
\begin{lstlisting}
Type type = typeof (Counter);
Counter c = new(1);
// All Fields
FieldInfo[] fiAll = type.GetFields(
	BindingFlags.Instance |
	BindingFlags.NonPublic);
// Specific Field
FieldInfo fi = type.GetField(
	"_countValue",
	BindingFlags.Instance |
	BindingFlags.NonPublic);
int val01 = (int) fi.GetValue(c);
c.Increment();
int val02 = (int) fi.GetValue(c);
fi.SetValue(c, -999);
\end{lstlisting}

\subsection{Property Info}
Die Property Info beschreibt eine Property einer Klasse (Name, Typ, Sichtbarkeit, Informationen zu Get/Set). Auch Properties lassen sich lesen und schreiben.
\begin{lstlisting}
Type type = typeof(Counter);
Counter c = new(1);
// All Properties
PropertyInfo[] piAll = type.GetProperties();
// Specific Property
PropertyInfo pi =
type.GetProperty("CountValue");
int val01 = (int)pi.GetValue(c);
c.Increment();
int val02 = (int)pi.GetValue(c);
if (pi.CanWrite) {
pi.SetValue(c, -999);
}

\end{lstlisting}
\subsection{Method Info}
Die Method Info beschreibt eine Methode einer Klasse (Name, Parameter, Rückgabewert, Sichtbarkeit). Sie leitet von Klasse MethodBase ab. Die Methode wird mit Invoke() aufgerufen.
\begin{lstlisting}
Type type = typeof(Counter);
Counter c = new(1);
// All Methods
MethodInfo[] miAll = type.GetMethods();
// Specific Method
MethodInfo mi = type.GetMethod("Increment");
mi.Invoke(c, null);
// Same with Parameters
Type type = typeof(System.Math);
Type[] paramTypes = { typeof(int) };
// Get method info for Cos( )
MethodInfo miAbs = type.GetMethod("Abs", paramTypes);
// Fill an array with the actual parameters
object[] @params = { -1 };
object returnVal = miAbs.Invoke(type, @params);
\end{lstlisting}
\subsection{Constructor Info}
Die Constructor Info beschreibt ein Konstruktor einer Klasse (Name, Parameter, Sichtbarkeit). Wie Method Info leitet er wegen seinen ähnlichen Eigenschaften von MethodBase ab und wird mit Invoke() aufgerufen.
\begin{lstlisting}
Type type = typeof(Counter);
// All Constructors
ConstructorInfo[] ciAll = type.GetConstructors();
// Specific Constructor Overload 01
ConstructorInfo ci01 = type.GetConstructor(
new[] { typeof(int) });
Counter c01 = (Counter)ci01.Invoke(
new object[] { 12 });
// Specific Constructor Overload 02
ConstructorInfo ci02 = type.GetConstructor(
BindingFlags.Instance|BindingFlags.NonPublic,
null, new Type[0], null);
Counter c02 = (Counter)ci02.Invoke(null);
// Alternative with Activator
Counter c03 = (Counter)Activator.CreateInstance(
	typeof(Counter), 12 // , "further params", "", ...);
// Alternative with Activator if Public default constructor exists
Counter c04 = Activator
.CreateInstance<Counter>();

\end{lstlisting}

\section{Attributes}
Attribute bieten die Möglichkeit, Informationen in deklarativer Form mit Code zu verknüpfen. Attribute können außerdem als wiederverwendbare Elemente genutzt werden, die auf eine Vielzahl von Zielen angewendet werden können.

\subsection{Anwendungsfälle}
\begin{itemize}
	\itemsep -0.5em
	\item Object-relationales Mapping
	\item Serialisierung (WCF, XML)
	\item Security und Zugriffssteuerung
	\item Dokumentation
\end{itemize}

\subsection{Custom Attributes}

\begin{lstlisting}
// Declaration
[AttributeUsage(
	AttributeTargets.Class |
	AttributeTargets.Constructor |
	AttributeTargets.Field |
	AttributeTargets.Method |
	AttributeTargets.Property,
	AllowMultiple = true)]
public class BugfixAttribute : Attribute {
	public BugfixAttribute(int bugId, string programmer, string date) { /* ... */ }
	public int BugId { get; }
	public string Date { get; }
	public string Programmer { get; }
	public string Comment { get; set; }
}
// Usage
[Bugfix(121, "Manuel Bauer", "01/03/2015")]
[Bugfix(107, "Manuel Bauer", "01/04/2015", Comment = "Some major changes!")]
public class MyMath {
	[Bugfix(121, "Manuel Bauer", "01/05/2015")]
	public int MyInt { get; set; }
	// Compilerfehler weil Member-Typ = Event
	[Bugfix(148, "Manuel Bauer", "01/06/2015")]
	public event Action CalculationDone;
}
// Check via Reflection
Type type = typeof(MyMath);
// All Class Attributes
object[] aiAll = type.GetCustomAttributes(true);
IEnumerable<Attribute> aiAllTyped =
	type.GetCustomAttributes(typeof(BugfixAttribute) /* , true */);
// Check Definition
bool aiDef = type.IsDefined(typeof(BugfixAttribute))
\end{lstlisting}


